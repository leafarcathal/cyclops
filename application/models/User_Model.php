<?php

require_once APPPATH.'libraries/My_Model.php';

class User_Model extends My_Model{

	protected $name = 'user';
	public $user_id;
	public $user_name;
	public $user_email;
	public $user_password;
	public $user_register_data;
	public $user_profile_img;
	public $user_nickname;
	public $register_user_id;

	public function __construct()
	{
		parent::__construct();
        $this->load->database();
	}

	public function getLoginByEmail($user_email, $user_password)
    {
        $usuario = $this->getByuser_emailByuser_passwordByuser_status(['user_id'],
                                                                   $user_email,
                                                                   $user_password,
                                                                   1);
        if(!$usuario)
            return ['erro' => 'Usuario nao encontrado.'];
        return $usuario;
    }

}
?>