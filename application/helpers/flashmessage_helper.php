<?php
// Private
// ####################################################
function getFlashMessageObject($message)
{
	$message = (object)$message;
	if(!isset($message->container))
		$message->container = 'flashmessage';
	if(!isset($message->html))
		$message->html = true;
	if(!isset($message->class))
		$message->class = 'alert-danger';
	if(!isset($message->close))
		$message->close = true;
	if(!isset($message->buttons))
		$message->buttons = false;
	if(!isset($message->redirect))
		$message->redirect = false;
	return $message;
}
function getFlashMessageHtml(stdClass $message)
{

	switch ($message->class) {
        case 'alert-success':
            $message->id = 'alert-success';
            break;
        case 'alert-warning':
            $message->id = 'alert-warning';
            break;
        default:
            $message->id = 'alert-error';
            break;
    }
  //   if($message->class == 'alert-success')
  //       $message->id = 'alert-success';
  //   else
		// $message->id = 'alert-error';

    if(!$message->html)
        return $message->text;
    if($message->close)
        $message->close = '<button data-dismiss="alert" class="close" type="button">×</button>';
    if($message->buttons){
        $message->id = 'alert-confirm';
        $botoes = '';
        foreach($message->buttons as $button){
            $button = (object)$button;
            $botoes .= '<a href="'.$button->href.'" class="btn '.$button->class.'" type="button">'.$button->label.'</a> ';
        }
		return <<<ABC
			<div id="$message->id" role="alert" class="alert $message->class fade in">
				<button data-dismiss="alert" class="close" type="button">
					<span aria-hidden="true">×</span>
					<span class="sr-only">Close</span>
				</button>
				<p>$message->text</p>
				<p>
					$botoes
				</p>
			</div>
ABC;
	}
	return <<<ABC
		<div id="$message->id" role="alert" class="alert $message->class">
			$message->close
			$message->text
		</div>
ABC;
}

// Public
// ####################################################
function addFlashMessage(array $message, $return = false)
{
	$ci =& get_instance();
	$message = getFlashMessageObject($message);
	$html = getFlashMessageHtml($message);

    // pega as mensagens anteriores para concatenar
    if($concat_message = getFlashMessage($message->container))
        $html = $html . $concat_message;

	if(!$return)
		$ci->session->set_userdata($message->container, $html);
	else
		return $html;
	if($message->redirect)
		redirect($message->redirect);
}
function getFlashMessage($container = 'flashmessage', $message = false)
{
	$ci =& get_instance();
	if(!$message)
		$message = $ci->session->userdata($container);
	$ci->session->unset_userdata($container);
	return $message;
}