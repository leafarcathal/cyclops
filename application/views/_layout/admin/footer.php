<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b>Version</b> 1.1.0
	</div>
	<strong>Copyright &copy; 2016 Leafar's IT Solutions.</strong> All rights reserved.
</footer>