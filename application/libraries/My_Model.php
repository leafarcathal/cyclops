<?php

class My_model extends CI_Model {

    private $uuid;
    private $utcTime;

    public function __construct()
    {
        parent::__construct();
    }

    public function getUtcTime($novo = false)
    {
        if(!$this->utcTime || $novo)
            $this->utcTime = time();
        return $this->utcTime;
    }

    public function getIso8601Data($time = false)
    {
        $time = $time ? $time : $this->getUtcTime();
        return gmdate('Y-m-d\TH:i:s:Z\Z', $time);
    }

    public function getMysqlUtcData($novo = false)
    {
        return gmdate('Y-m-d H:i:s', $this->getUtcTime());
    }

    public function getUuid($novo = false)
    {
        if(!$novo && $this->uuid)
            return $this->uuid;
        return $this->uuid = md5(uniqid(rand(), true));
    }

    public function _save($metodo, $parametros)
    {
        $dados = $parametros[0];
        $id = isset($parametros[1]) ? $parametros[1] : false;
        $add = true;
        if(count($parametros) > 1){
            if(is_numeric($id)){
                // Upd
                // Verifica se o registro existe na base de dados
                $registro = $this->db->select($this->name . '_id')
                                     ->from($this->name)
                                     ->where($this->name . '_id', $id);
                if($registro){
                    $add = false;
                }else
                    return;
            }
        }
        // Add
        if($add){
            if(property_exists(get_class($this), $this->name.'_inclusao'))
            if($this->db->insert($this->name, $dados))
                return $this->db->insert_id();
            return;
        }
        // Upd
        if(property_exists(get_class($this), $this->name.'_alteracao'))
        if($this->db->update($this->name, $dados, [$this->name.'_id' => $id]));
            return $id;
        return;
    }

    public function __call($metodo, $parametros)
    {
        if(stripos($metodo, 'get') === 0)
            return $this->_get($metodo, $parametros);
        if(stripos($metodo, 'save') === 0)
            return $this->_save($metodo, $parametros);
    }

    public function _get($metodo, $parametros)
    {
        $metodo = strtolower($metodo);
        $all = (stripos($metodo, 'all') === false ? false : true);
        $ordens = array();
        // $parametros = $parametros[0];
        // Select
        $colunas = explode('by', $metodo);
        array_shift($colunas);
        if(count($parametros) > count($colunas)){
            $colunas = $parametros[0];
            unset($parametros[0]);
        }else{
                $colunas = $this->name.'_id';
            $parametros = $parametros;
        }   
        $posicaoOrderBy = stripos($metodo, 'orderby');
        if($posicaoOrderBy !== false){
            $ordersBy = substr($metodo, stripos($metodo, 'orderby'));
            $ordersBy = explode('orderby', $ordersBy);
            $metodo = substr($metodo, 0, $posicaoOrderBy);
            foreach ($ordersBy as $key => $legenda) {
                if($legenda){
                    if(stripos($legenda, 'desc') !== false){
                        $ordem = substr($legenda, 0, 4);
                        $legenda = substr($legenda, 4);
                    }else{
                        $ordem = 'ASC';
                        if(stripos($legenda, 'asc') !== false)
                            $legenda = substr($legenda, 3);
                    }
                    $ordens[$legenda] = $ordem;
                }
            }
        }
        $metodo = explode('by', $metodo);
        // Procura por get, getall
        for($i = 0; $i <= count($metodo); $i++){
            if($metodo[$i] == 'get' || $metodo[$i] == 'getall')
                unset($metodo[$i]);
        }
        if(!count($metodo))
            $metodo[0] = $this->name.'_id';
        if(count($metodo) != count($parametros))
            return ['erro' => 'Formato do metodo invalido.'];
        $where = array_combine($metodo, $parametros);
        foreach($ordens as $campo => $ordem)
            $this->db->order_by($campo, $ordem);
        if(!$all)
            $this->db->limit(1);
        $query = $this->db->select($colunas)
                          ->from($this->name)
                          ->where($where)
                          ->get();
        if(!$all)
            return $query->first_row();
    }

}